{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "# Tutorial 1: CommonRoad-io\n",
    "## Reading, Modifying, and Writing Common Scenarios\n",
    "\n",
    "This tutorial shows how CommonRoad XML-files can be read, modified, visualized, and stored. To start with, a CommonRoad XML-file consists of a **Scenario** and a **PlanningProblemSet**:\n",
    "* A **Scenario** represents the environment including a **LaneletNetwork** and a set of **DynamicObstacle** and **StaticObstacle**.\n",
    "    * A **LaneletNetwork** is built from lane segments (**Lanelet**), that can be connected arbitrarily.\n",
    "* A **PlanningProblemSet** contains one **PlanningProblem** for every ego vehicle in the **Scenario**, consisting of an **initial position** and a **GoalRegion** that has to be reached."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## 0. Preparation\n",
    "* Before you proceed any further, make sure you have skimmed through [CommonRoad API](https://commonroad-io.readthedocs.io/en/latest/api/) to gain an overall view of the functionalities provided by CommonRoad modules. You may need to refer to it for implementation details throughout this tutorial.\n",
    "\n",
    "* Additional documentations on **CommonRoad XML format, Cost Functions, Vehicle Models, etc.** can be found at [CommonRoad](https://commonroad.in.tum.de/) on the specific tool pages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## 1. Read XML file\n",
    "\n",
    "As documented in [CommonRoadFileReader](https://commonroad-io.readthedocs.io/en/latest/api/common/#module-commonroad.common.file_reader), the **CommonRoadFileReader** reads in a CommonRoad XML file, and its **open()** method returns a **Scenario** and a **PlanningProblemSet** object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# import functions to read xml file and visualize commonroad objects\n",
    "from commonroad.common.file_reader import CommonRoadFileReader\n",
    "from commonroad.visualization.mp_renderer import MPRenderer\n",
    "\n",
    "# generate path of the file to be opened\n",
    "file_path = \"ZAM_Tutorial-1_1_T-1.xml\"\n",
    "\n",
    "# read in the scenario and planning problem set\n",
    "scenario, planning_problem_set = CommonRoadFileReader(file_path).open()\n",
    "\n",
    "# plot the scenario for 40 time step, here each time step corresponds to 0.1 second\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    rnd = MPRenderer()\n",
    "    # set time step in draw_params\n",
    "    rnd.draw_params.time_begin = i\n",
    "    # plot the scenario at different time step\n",
    "    scenario.draw(rnd)\n",
    "    # plot the planning problem set\n",
    "    planning_problem_set.draw(rnd)\n",
    "    rnd.render()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## 2. Modify XML file\n",
    "\n",
    "It is possible to modify existing CommonRoad scenarios to customize them to one's need. First, we would like to add a static obstacle to the scenario with the following specification:\n",
    "\n",
    "    - obstacle type: parked vehicle\n",
    "    - obstacle shape: rectangle, with a width of 2.0m and a length of 4.5m\n",
    "    - initial state:\n",
    "        - position: (30, 3.5) m\n",
    "        - orientation: 0.02 rad\n",
    "* obstacle id: since every object in the scenario must have a unique ID, we can use the member function **generate_object_id** of **Scenario** class to generate a unique ID for the object.\n",
    "\n",
    "As documented in [StaticObstacle](https://commonroad-io.readthedocs.io/en/latest/api/scenario/#staticobstacle-class), we need to provide `obstacle_id, obstacle_type, obstacle_shape, initial_state` to construct a static obstacle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "# import necessary classes from different modules\n",
    "from commonroad.geometry.shape import Rectangle\n",
    "from commonroad.scenario.obstacle import StaticObstacle, ObstacleType\n",
    "from commonroad.scenario.state import InitialState\n",
    "\n",
    "# read in the scenario and planning problem set\n",
    "scenario, planning_problem_set = CommonRoadFileReader(file_path).open()\n",
    "\n",
    "# generate the static obstacle according to the specification, refer to API for details of input parameters\n",
    "static_obstacle_id = scenario.generate_object_id()\n",
    "static_obstacle_type = ObstacleType.PARKED_VEHICLE\n",
    "static_obstacle_shape = Rectangle(width = 2.0, length = 4.5)\n",
    "static_obstacle_initial_state = InitialState(position = np.array([30.0, 3.5]), orientation = 0.02, time_step = 0, velocity=0, acceleration=0, yaw_rate=0, slip_angle=0)\n",
    "\n",
    "# feed in the required components to construct a static obstacle\n",
    "static_obstacle = StaticObstacle(static_obstacle_id, static_obstacle_type, static_obstacle_shape, static_obstacle_initial_state)\n",
    "\n",
    "# add the static obstacle to the scenario\n",
    "scenario.add_objects(static_obstacle)\n",
    "\n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 41):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    rnd = MPRenderer()\n",
    "    rnd.draw_params.time_begin = i\n",
    "    scenario.draw(rnd)\n",
    "    planning_problem_set.draw(rnd)\n",
    "    rnd.render()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "As can be seen, we have added a new static obstacle to the scenario. We can further add a dynamic obstacle with the following specifications:\n",
    "\n",
    "    - obstacle type: car\n",
    "    - obstacle shape: rectangle with a width of 1.8m and a length of 4.3m\n",
    "    - initial state:\n",
    "        - position: (50, 0.0) m\n",
    "        - orientation: 0.00 rad\n",
    "        - velocity: 22 m/s along x axis\n",
    "    - we assume that the dynamic obstacle drives with constant velocity.\n",
    "\n",
    "As documented in [DynamicObstacle](https://commonroad-io.readthedocs.io/en/latest/api/scenario/#dynamicobstacle-class), we need to pass in a **Prediction** object which in this case is a **TrajectoryPrediction** object. Its generation goes as follows:\n",
    "    1. compute all subsequent states for the dynamic obstacle\n",
    "    2. create a Trajectory from these states\n",
    "    3. create a TrajectoryPrediction from this trajectory and obstacle shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# import necessary classes from different modules\n",
    "from commonroad.scenario.obstacle import DynamicObstacle\n",
    "from commonroad.scenario.state import CustomState\n",
    "from commonroad.scenario.trajectory import Trajectory\n",
    "from commonroad.prediction.prediction import TrajectoryPrediction\n",
    "\n",
    "# initial state has a time step of 0\n",
    "dynamic_obstacle_initial_state = CustomState(position = np.array([50.0, 0.0]),\n",
    "                                             velocity = 22,\n",
    "                                             orientation = 0.02,\n",
    "                                             time_step = 0).convert_state_to_state(InitialState())\n",
    "\n",
    "# generate the states for the obstacle for time steps 1 to 40 by assuming constant velocity\n",
    "state_list = []\n",
    "for i in range(1, 41):\n",
    "    # compute new position\n",
    "    new_position = np.array([dynamic_obstacle_initial_state.position[0] + scenario.dt * i * 22, 0])\n",
    "    # create new state\n",
    "    new_state = CustomState(position = new_position, velocity = 22,orientation = 0.02, time_step = i)\n",
    "    # add new state to state_list\n",
    "    state_list.append(new_state)\n",
    "\n",
    "# create the trajectory of the obstacle, starting at time step 1\n",
    "dynamic_obstacle_trajectory = Trajectory(1, state_list)\n",
    "\n",
    "# create the prediction using the trajectory and the shape of the obstacle\n",
    "dynamic_obstacle_shape = Rectangle(width = 1.8, length = 4.3)\n",
    "dynamic_obstacle_prediction = TrajectoryPrediction(dynamic_obstacle_trajectory, dynamic_obstacle_shape)\n",
    "\n",
    "# generate the dynamic obstacle according to the specification\n",
    "dynamic_obstacle_id = scenario.generate_object_id()\n",
    "dynamic_obstacle_type = ObstacleType.CAR\n",
    "dynamic_obstacle = DynamicObstacle(dynamic_obstacle_id, \n",
    "                                   dynamic_obstacle_type, \n",
    "                                   dynamic_obstacle_shape, \n",
    "                                   dynamic_obstacle_initial_state, \n",
    "                                   dynamic_obstacle_prediction)\n",
    "\n",
    "# add dynamic obstacle to the scenario\n",
    "scenario.add_objects(dynamic_obstacle)\n",
    "\n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 41):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    rnd = MPRenderer()\n",
    "    rnd.draw_params.time_begin = i\n",
    "    scenario.draw(rnd)\n",
    "    planning_problem_set.draw(rnd)\n",
    "    rnd.render()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## 3. Write XML file\n",
    "After we have modified the scenario, we would like to save the changes and write the **Scenario** and **PlanningProblemSet** to a CommonRoad XML file. [CommonRoadFileWriter](https://commonroad-io.readthedocs.io/en/latest/api/common/#commonroadfilewriter-class) helps us with this purpose.\n",
    "Note that we did not modify the **PlanningProblemSet** in this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "# import necessary classes from different modules\n",
    "from commonroad.common.file_writer import CommonRoadFileWriter\n",
    "from commonroad.common.file_writer import OverwriteExistingFile\n",
    "from commonroad.scenario.scenario import Tag\n",
    "\n",
    "author = 'Max Mustermann'\n",
    "affiliation = 'Technical University of Munich, Germany'\n",
    "source = ''\n",
    "tags = {Tag.CRITICAL, Tag.INTERSTATE}\n",
    "\n",
    "# write new scenario\n",
    "fw = CommonRoadFileWriter(scenario, planning_problem_set, author, affiliation, source, tags)\n",
    "\n",
    "filename = \"ZAM_Tutorial-1_2_T-1.xml\"\n",
    "fw.write_to_file(filename, OverwriteExistingFile.ALWAYS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "We can open our stored file again to check if everything is correct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "file_path = \"ZAM_Tutorial-1_2_T-1.xml\"\n",
    "\n",
    "scenario, planning_problem_set = CommonRoadFileReader(file_path).open()\n",
    "\n",
    "# plot the scenario for each time step\n",
    "for i in range(0, 40):\n",
    "    plt.figure(figsize=(25, 10))\n",
    "    rnd = MPRenderer()\n",
    "    rnd.draw_params.time_begin = i\n",
    "    scenario.draw(rnd)\n",
    "    planning_problem_set.draw(rnd)\n",
    "    rnd.render()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "outputs": [],
   "source": [],
   "metadata": {
    "collapsed": false,
    "pycharm": {
     "name": "#%%\n"
    }
   }
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
